import Vue from 'vue'
import router from './store/router'
import store from './store/store'
import App from './App.vue'

import VueCryptojs from 'vue-cryptojs'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(VueCryptojs)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  template: '<App/>',
  component: { App },
  router,
  store
}).$mount('#app')
