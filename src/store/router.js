import Vue from 'vue'
import Router from 'vue-router'
import middleware from './middleware'
import store from './store'

// import { Crypto } from "cryptojs";
const Unauthorized = () => import('@/views/Unauthorized')
const LoginForm = () => import('@/views/LoginForm')
const Analytics = () => import('@/views/Analytics/Index')
const Task = () => import('@/views/Task/Index')
const LandingPage = () => import('@/views/Task/LandingPage')
const TaskDetail = () => import('@/views/Task/Detail')
const ConfigUsers = () => import('@/views/Configurations/Users/List')
const ConfigUserDetail = () => import('@/views/Configurations/Users/Detail')
const ConfigProducts = () => import('@/views/Configurations/Products/List')
const ConfigProductDetail = () => import('@/views/Configurations/Products/Detail')

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/unauthorized',
      name: 'unauthorized',
      component: Unauthorized,
      noAuth: true
    },
    {
      path: '/login',
      name: 'login',
      component: LoginForm,
      noAuth: true
    },
    {
      path: '/analytics',
      name: 'analytics',
      component: Analytics,
      meta: {
        permission: ['admin']
      }
    },
    // {
    //   path: '/task',
    //   name: 'task',
    //   component: ''
    // }
    {
      path: '/conf/users',
      name: 'users',
      component: ConfigUsers,
      meta: {
        permission: ['admin']
      }
    },
    {
      path: '/conf/users/detail',
      name: 'user-detail',
      component: ConfigUserDetail,
      meta: {
        permission: ['admin']
      }
    },
    {
      path: '/conf/products',
      name: 'products',
      component: ConfigProducts,
      meta: {
        permission: ['admin']
      }
    },
    {
      path: '/conf/products/detail',
      name: 'product-detail',
      component: ConfigProductDetail,
      meta: {
        permission: ['admin']
      }
    },
    {
      path: '/task',
      name: 'task',
      component: Task
    },
    {
      path: '/task/detail',
      name: 'task-detail',
      component: TaskDetail
    },
    {
      path: '/landing-page',
      name: 'landing-page',
      component: LandingPage,
      meta: {
        permission: ['admin','marketing']
      }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
      }
    }

    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
})

middleware(router, store)

export const isExpiredAccess = () => {
  let jwtToken = localStorage.getItem('jwt_token')
  if (!jwtToken) {
    return true
  }
  jwtToken = Vue.CryptoJS.AES.decrypt(jwtToken, 'SecretDemoOrderManager').toString(Vue.CryptoJS.enc.Utf8)
  try {
    return Number(jwtToken) < new Date().getTime()
  } catch (e) {
    return true
  }
}

export default router
