import Vue from 'vue'
import Vuex from 'vuex'
import '@/constants'
import {ROLE_NAME} from "@/constants";
import {httpService} from "@/services/httpService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {},
    users:[],
    orders: [],
    products:[],
    permission:{},
    accessToken: null,
    apiToken: null,
    hardCodeData: {
      roles: {
        admin: ROLE_NAME.ADMIN,
        marketing: ROLE_NAME.MARKETING,
        telesales: ROLE_NAME.TELESALES,
        warehouse: ROLE_NAME.WAREHOUSE,
      },
      fulfillStatus: [
        // {value: "CANCEL", text: 'Huỷ'},
        // {value: "READY TO PICK", text: 'Chờ Lấy hàng'},
        {value: "STORING", text: 'Lưu kho'},
        // {value: "PICKING", text: 'Đã lấy'},
        {value: "SHIPPED", text: 'GHN đã lấy'},
        {value: "RETURNED", text: 'Trả hàng thành công'},
        {value: "RETURN", text: 'Chờ trả lại'},
        {value: "DELIVERING", text: 'Đang giao'},
        {value: "DELIVERED", text: 'Giao thành công'},
        {value: "DELAYED", text: 'Chờ trả hàng'},
        // {value: "WAIT TO FINISH", text: 'Tổng kết hoàn/chờ chuyển COD'},
        {value: "FINISH", text: 'Hoàn Tất'},
        {value: "LOCAL_AWAITING_SHIPMENT", text: 'Đang giao hàng'},
        {value: "LOCAL_WAIT_COD", text: 'Chờ chuyển COD'},
        {value: "LOCAL_WAIT_PAYMENT", text: 'Chờ thanh toán'},
      ],
      systemStatus: [
        {value: 'FAILED', text: 'Tạo đơn GHN thất bại'},
        {value: 'CREATING', text: 'Đang xuất kho'},
        {value: 'ReadyToPick', text: 'Chờ Lấy hàng'},
      ],
      callStatus: [
        { value: "DUPLICATED", text: 'Trùng'},
        { value: "WRONG_PHONE_NUMB", text: 'Sai số'},
        { value: "NOT_PICKED_PHONE", text: 'Không nghe'},
        { value: "NOT_BUYING", text: 'Không mua'},
        { value: "CANCELED", text: 'Hủy'},
        { value: "BUYER_CONFIRMED", text: 'Đã chốt'},
        // { id: 14, name: 'Đang hoàn', type: 'call'},
      ],
      shippingService: [
        {text: 'GHN', value: 'ghn', name: 'GHN'},
        {text: 'Nội thành HN', value: 'local', name: 'Nội thành HN'},
      ]
    }
  },

  mutations: {
    updateAccessToken: (state, user) => {
      state.user = user
      state.accessToken = user.accessToken
      state.apiToken = user.apiToken
    },
    logout: (state) => {
      state.user = {}
      state.accessToken = ''
      state.apiToken = ''
      localStorage.setItem('jwt_token', '')
      localStorage.setItem('api_token', '')
      localStorage.setItem('session_user', '')
    },
    setStateProducts: (state, arr) => {
      state.products = arr
    },
    setStateUsers: (state, arr) => {
      state.users = arr
    },
    pushOrderVuex: (state, arr) => {
      state.orders = arr
    }
  },

  actions: {
    async fetchProducts({ commit }) {
      const res = await httpService.goRequest('/products')
      let rs = null
      if (res.result && res.result.length) {
        rs = res.result
      }
      commit('setStateProducts', rs)
      return rs
    },

    async fetchUsers({ commit }) {
      const res = await httpService.goRequest('/users')
      let rs = null
      if (res.result && res.result.length) {
        rs = res.result
      }
      commit('setStateUsers', rs)
      return rs
    }
  }
})
