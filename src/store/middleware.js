import {isExpiredAccess} from "@/store/router";

const middleware = (router, store) => {
  router.afterEach(async (to, from) => {
    if (to.name === from.name) return
    if (isExpiredAccess()) {
      if (to.name === 'login') return
      router.push({
        name: 'login'
      })
      return
    }

    if (store.state.user && store.state.user.role) {
      if (to.meta && to.meta.permission && to.meta.permission.length && !to.noAuth) {
        if (!to.meta.permission.includes(store.state.user.role.toLowerCase())) {
          router.push({
            name: 'unauthorized'
          })
        }
      }
    }
  })
}

export default middleware
