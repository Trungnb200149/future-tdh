import { IPConfig } from "@/store/constant";
import EventBus from "@/EventBus";

var api_token = null
const DEFAULT_TIMEOUT = 30000
export const httpService = {
  buildQueryPathByParams: (url, params = null, useEncode = false) => {
    if (!params || typeof params !== 'object' || Object.keys(params).length === 0) {
      return url
    }
    let first = true
    for (const key in params) {
      url += `${ first ? '?' : '&' }${key}=${useEncode ? encodeURIComponent(params[key]) : params[key]}`
      if (first) {
        first = false
      }
    }
    return url
  },
  goRequest: async (url, method = 'GET', body = null, isExternalRequest = false) => {
    let to = url
    let parsedBody = null
    const controller = new AbortController()
    const ms = DEFAULT_TIMEOUT
    const headers = {}

    if (body !== null) {
      parsedBody = JSON.stringify(body)
    }

    if (to.indexOf('http') !== 0) {
      to = `${IPConfig}${url}`
    }

    if (!isExternalRequest) {
      if (!api_token) {
        api_token = localStorage.getItem('api_token') || ''
      }
      headers['SH-AUTH-TOKEN'] = api_token
    }

    let res
    await timeout(fetch(to,  {
      method: method.toUpperCase(),
      body: parsedBody,
      mode: 'cors',
      headers
    }), controller, ms)
      .then(rs => {
        if (rs.status !== 200) {
          if (rs.status === 401) {
            EventBus.$emit('unauthorized')
            throw new TypeError(rs.statusText)
          } else if (rs.status >= 500) {
            EventBus.$emit('serverError')
            throw new TypeError(rs.statusText)
          } else {
            rs.error = 'Somethings went wrong!'
          }
        }
        res = rs
    })
      .catch((error) => {
        res = { error}
        // EventBus.$emit('unauthorized')
      })
    if (res.error) {
      return res
    } else {
      let parseObj
      await res.json().then( rs => parseObj = rs).catch((error) => parseObj = {error})
      return parseObj
    }
  }
}

export function timeout(promise, controller, ms) {
  let timeout
  return new Promise(function(resolve, reject) {
    timeout = setTimeout(function() {
      if (controller) {
        controller.abort()
      }

      reject(new Error('Request timeout'))
    }, ms)

    promise.then(resolve, reject).finally(() => {
      if (timeout) {
        clearTimeout(timeout)
      }
    })
  })
}

export const delCacheToken = () => {
  api_token = null
}
