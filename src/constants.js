export const ROLE_NAME = {
  ADMIN: 'Admin',
  PRODUCT: 'Product',
  TELESALES: 'Telesales',
  MARKETING: 'Marketing',
  WAREHOUSE: 'Warehouse'
}
