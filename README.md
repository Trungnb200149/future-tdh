# Store TDH Management

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Structure

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
